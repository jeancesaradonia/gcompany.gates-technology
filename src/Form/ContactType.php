<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom', TextType::class, [
                'label' => false,
                'required'  => true,
                'attr' => [
                    'placeholder' => 'Nom *',
                    'class' => 'form-control mb-3 nom-form',
                    'style' => 'border: 1px solid #DC2042;background-color: darkgray;',
                ]
            ])
            ->add('telephone', TextType::class, [
            'label' => false,
                'required'  => true,
                'attr' => [
                    'placeholder' => 'Telephone *',
                    'class' => 'form-control mb-3 telephone-form',
                'style' => 'border: 1px solid #DC2042;background-color: darkgray;',
                ]
            ])
            ->add('email', TextType::class, [
            'label' => false,
                'required'  => true,
                'attr' => [
                    'placeholder' => 'Email *',
                    'class' => 'form-control mb-3 email-form',
                'style' => 'border: 1px solid #DC2042;background-color: darkgray;',
                ]
            ])
            ->add('sujet', TextType::class, [
            'label' => false,
                'required'  => true,
                'attr' => [
                    'placeholder' => 'Sujet *',
                    'class' => 'form-control mb-3 sujet-form',
                'style' => 'border: 1px solid #DC2042;background-color: darkgray;',
                ]
            ])
            ->add('message', TextareaType::class, [
            'label' => false,
                'required'  => true,
                'attr' => [
                    'placeholder' => 'Message *',
                    'class' => 'form-control mb-3 message-form',
                    'rows' => '3',
                    'style' => 'resize: none;border: 1px solid #DC2042;background-color: darkgray;',
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}

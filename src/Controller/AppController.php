<?php

namespace App\Controller;

use App\Form\ContactType;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AppController extends AbstractController
{
    /**
     * @Route("/{lang}", name="app_home")
     */
    public function index(Request $request, MailerInterface $mailer, $lang = "en"): Response
    {
        $contactForm = $this->createForm(ContactType::class);
        $contactForm->handleRequest($request);
       
        if ($contactForm->isSubmitted() && $contactForm->isValid()) {
            extract($contactForm->getData());
            $email = (new TemplatedEmail())
                ->from($email)
                ->to("contact@gate-digital.com")
                ->subject($sujet)
                ->text("Nom : " . $nom . '<br />Message : ' . $message);
            $mailer->send($email);
        } 
        return $this->render('app/'.$lang.'.html.twig', [
            "form" => $contactForm->createView()
        ]);
    }


    /**
     * @Route("/send-contact-mail/", name="app_send_contact_mail")
     */
    public function sendContactMail(Request $request ,MailerInterface $mailer)
    {
            $nom = $request->request->get('nom', "");
            $telephone = $request->request->get('telephone', "");
            $email = $request->request->get('email', "");
            $sujet = $request->request->get('sujet', "");
            $message = $request->request->get('message', "");
            $email = (new TemplatedEmail())
                ->from($email)
                ->to("contact@gate-digital.com")
                ->subject($sujet)
                ->text("Nom & Téléphone: " . $nom .' & '. $telephone . '<br />Message : ' . $message);
            $mailer->send($email);
            return $this->json([
                "code" => "302",
                "message" => "E-mail bien envoyé",
            ]);
    }
    

}

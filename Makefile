# VARIABLE

PWD:
	pwd


# LES COMMANDES

start-server:
	docker-compose up -d
	symfony serve -d
	symfony open:local
.PHONY: start-server


stop-server:
	docker-compose down --remove-orphans
	symfony server:stop
.PHONY: stop-server

php-7:
	docker run -it --rm -v "$(PWD)":/gates-technology/project -w /gates-technology/project gate:1 bash
.PHONY: php-7
